"""
# 
#  @author CHEN Qing
# 
#  Compilation:  ---
#  Execution:    myyade -j4 06_example_periodicTriaxial_rollingcohensiv.py
#  Dependencies: yade ( self compilation version and the alias is myyade )
#
#  drained triaxial shearing test (for pratice purpose)
#
"""
  
from yade import pack, qt, plot, utils

## define material , a.k.a., contact model
youngModule = 3e9
poissonRatio = 0.2
packFircAngle = 5       # 5 or smaller for loose sample and 30 or higher for dense sample
realFircAngle = 30   

#parM=O.materials.append(FrictMat(young=youngModule,poisson=poissonRatio,frictionAngle=radians(packFircAngle),label="parM"))
parMat=O.materials.append(CohFrictMat(
        young=youngModule,
        poisson=poissonRatio,
        frictionAngle=radians(packFircAngle),
        isCohesive=True,
        normalCohesion = 1e18,
        shearCohesion = 1e18,# test this value tomorrow
        momentRotationLaw=False,
        alphaKr = 0.5,
        alphaKtw = 0.5,
        etaRoll = 0.5,
        etaTwist = 0.5,
        label="parMat")  )
#// may be try to use FrictMatCDM or some bonded model

## control triaxial process
sigmaIso = -100e3
threhold_cofi = 0.90
compressRate = 10
shearingRate = 0.1
## control box size 
height = 2
width  = 2
length = 2
## control pack void ratio / porosity
v_ratio = 0.75
porosity = v_ratio/(1+v_ratio)
#porosity = 0.68
## control particle size distribution
R_factor = 0.3
pS = [	0.25 * R_factor ,
	0.30 * R_factor ,
	0.35 * R_factor ,
	0.40 * R_factor ,
	0.45 * R_factor ,
	0.50 * R_factor ,
	0.55 * R_factor ,
	0.60 * R_factor ,
	0.65 * R_factor ,
	0.70 * R_factor ,
	0.75 * R_factor
]
pC = [	0.0,
	0.015625,
	0.046875,
	0.09375,
	0.1875,
	0.3125,
	0.484375,
	0.671875,
	0.84375,
	0.9375,
	1.000
]

## generate particles and pack 
O.periodic = True
sp = pack.SpherePack()
minCon,maxCon=Vector3(0,0,0),Vector3(length,width,height)
snum=sp.makeCloud(minCon,maxCon,psdSizes=pS,psdCumm=pC,porosity=porosity,periodic=True,seed=3407)
#sp.toSimulation()
#O.bodies.append([sphere(center,rad,material='parMat') for center,rad in sp])
sp.toSimulation(material='parMat')
print("============================================")
if(snum == len(O.bodies)):
        print(len(O.bodies),"have been generate.")

print("============================================")
def checkInteraction():
        realIntCount = 0
        activeCount = 0
        for i in O.interactions:
                if (i.isReal):
                        realIntCount += 1
                if (i.isActive):
                        activeCount += 1
                tmpIntr1 = i.id1
                tmpIntr2 = i.id2
                #print(i.phys.normalAdhesion, "is some contact's normal cohesion")
        print(realIntCount, "real interaction have been generate")
        print(activeCount, "active interaction have been generate")
        #return realIntCount
        
checkInteraction()

## define triaxial controller

denseAssembly = PeriTriaxController(
                label='triax',
                # specify target values and whether they are strains or stresses
                # x,y,z is 3 binary bit, and 1 for stress 0 for strain 
                # so, all stress is 111 = 2**2 + 2**1 +2**0 = 4 + 2 + 1
                goal=(sigmaIso*threhold_cofi, sigmaIso*threhold_cofi, sigmaIso*threhold_cofi),
                stressMask=7,
                # type of servo-control
                dynCell=True,
                maxStrainRate=(compressRate, compressRate, compressRate),
                # wait until the unbalanced force goes below this value
                maxUnbalanced=.1,
                relStressTol=1e-3,
                # call this function when goal is reached and the packing is stable
                doneHook='compression()'
        )
 

## define engines
O.engines = [
        ForceResetter(),
        InsertionSortCollider([Bo1_Sphere_Aabb(aabbEnlargeFactor=1.2)]),
        #InsertionSortCollider([Bo1_Sphere_Aabb()]),
        InteractionLoop(
            [Ig2_Sphere_Sphere_ScGeom6D(interactionDetectionFactor=1.2)], 
            #[Ig2_Sphere_Sphere_ScGeom6D()], 
            [Ip2_CohFrictMat_CohFrictMat_CohFrictPhys(setCohesionNow=True)], 
            [Law2_ScGeom6D_CohFrictPhys_CohesionMoment(
                always_use_moment_law=True,
		useIncrementalForm=True)] 
        ),
        denseAssembly,
        NewtonIntegrator(damping=.2),
        #PyRunner(command='history()', iterPeriod=5000),
]
O.dt = .5 * PWaveTimeStep()
O.step()
## enable energy tracking in the code
O.trackEnergy = True



## start simulation automatically
#O.run()
 

########################################################################################################
'''below this line is some functions that would call by the simulation'''
########################################################################################################



def compression():
        print("============================================")
        checkInteraction()
        print('dense assembly have generated')
        print('the confining stresss is ', ( triax.stress[0]+triax.stress[1]+triax.stress[2])/3 )
        print('the friction of assembly is ', (O.materials['parMat'].frictionAngle))
        O.materials['parMat'].frictionAngle=radians(realFircAngle)# it work, no need to specify the particle material
        triax.goal=(sigmaIso, sigmaIso, sigmaIso)
        triax.stressMask=7
        # type of servo-control
        triax.dynCell=True
        triax.maxStrainRate=(compressRate, compressRate, compressRate)
        # wait until the unbalanced force goes below this value
        triax.maxUnbalanced=.1
        triax.relStressTol=1e-3
        # call this function when goal is reached and the packing is stable
        triax.doneHook='shear()'

def shear():
        print("============================================")
        print('the confining stresss is ', ( triax.stress[0]+triax.stress[1]+triax.stress[2])/3 )
        print('the friction of assembly is ', (O.materials['parMat'].frictionAngle))
        print('shearing begin:')
        #O.materials['parMat'].alphaKr=2.0
        #O.materials['parMat'].alphaKtw=2.0
        #O.materials['parMat'].etaRoll=0.3
        #O.materials['parMat'].etaTwist=0.3
        #O.materials['parMat'].normalCohesion = 1e20
        #O.materials['parMat'].shearCohesion = 1e20
        #O.materials['parMat'].momentRotationLaw=True
        #O.engines[2] = InteractionLoop(
        #    [Ig2_Sphere_Sphere_ScGeom6D(interactionDetectionFactor=1.2)], 
        #    [Ip2_CohFrictMat_CohFrictMat_CohFrictPhys(setCohesionNow=True)], 
        #    [Law2_ScGeom6D_CohFrictPhys_CohesionMoment(
        #        always_use_moment_law=True,
	#	useIncrementalForm=True)] 
        #)
        checkInteraction()
        # set the current cell configuration to be the reference one
        O.cell.trsf = Matrix3.Identity
        # change control type: keep constant confinement in x,y, 20% compression in z
        triax.goal = (sigmaIso, sigmaIso, -0.3)
        triax.stressMask = 3
        # allow faster deformation along x,y to better maintain stresses
        triax.maxStrainRate = (shearingRate, shearingRate, .1)
        # next time, call triaxFinished instead of compactionFinished
        triax.doneHook = 'triaxFinished()'
        # do not wait for stabilization before calling triaxFinished
        triax.maxUnbalanced = 10
        O.engines += [PyRunner(command='history()',iterPeriod=15000)]
        ## O.engines += [PyRunner(command='savedata()',iterPeriod=10000)]
        # IndentationError: unindent does not match any outer indentation level, 
        # abova line used to have a strange python error
 
def pFromStressTensor():
        p = 0
        Tstress = utils.normalShearStressTensors()[0]+utils.normalShearStressTensors()[1]
        p = (Tstress[0][0]+Tstress[1][1])/2.0
        return p

def qFromStressTensor():
        q = 0
        Tstress = utils.normalShearStressTensors()[0]+utils.normalShearStressTensors()[1]
        q = Tstress[2][2] - (Tstress[0][0]+Tstress[1][1])/2.0
        return q

def history():
	plot.addData(
		e11=triax.strain[0], 
		e22=triax.strain[1], 
		e33=triax.strain[2],
		s11=triax.stress[0],
		s22=triax.stress[1],
		s33=triax.stress[2],
		i=O.iter,
		a_strain = -triax.strain[2],
		c_stress = -(triax.stress[0]+triax.stress[1])/2,
		d_stress = -(triax.stress[2] - ((triax.stress[0]+triax.stress[1])/2.0) ),
		volumtric_strain = ( triax.strain[2]+triax.strain[1]+triax.strain[0] ) ,
                unbalForce = triax.currUnbalanced,
                poro = utils.porosity(),
                coordNum = utils.avgNumInteractions(),
                stressTn = utils.normalShearStressTensors()[0],
                stressTs = utils.normalShearStressTensors()[1],
                fabricT = utils.fabricTensor()[0],
                p = pFromStressTensor(),
                q = qFromStressTensor()
                )

## define what to plot
def definePlot():
        plot.plots = {
                ' i ': ('unbalForce', 'a_strain'),
                'a_strain': ('volumtric_strain' ),
                # energy plot
                'a_strain ': ('d_stress', 'c_stress', None, 'q', 'p'),
                'a_strain  ': ('d_stress', 'c_stress'),
        }

saving_num = 0
####################################################
def savedata():
        global saving_num
        saving_num += 1
        #print "testing, no data saved"
        O.save('shear'+str(saving_num)+'.xml.bz2')


def triaxFinished():
        print("============================================")
        print('the confining stresss is ', (-(triax.stress[0]+triax.stress[1])/2) )
        print('the deviatoric stress is ', -(triax.stress[2] - ((triax.stress[0]+triax.stress[1])/2.0) ) )
        print('the volluntric strain is ', ( triax.strain[2]+triax.strain[1]+triax.strain[0] ) )
        print('Shearing Finished!')
        # show the plot
        definePlot()
        plot.plot(subPlots=False)
        O.pause()
        O.save('periodiTriaixal.yade.bz2')

"""
for i in range(1,10): 
    ...:     O.load("shear"+str(i)+".xml.bz2") 
    ...:     print(triax.stress[2]) 
    ...:                                                                                                                                            
-399798.9891073499
-587506.5878131134
-580798.7027900304
-713277.1380657047
-672858.7342332047
-885158.0830078434
-872543.3286160384
-739644.1873199734
-742318.9424270276

! muti threads , the result is not dterminstic
below is 3 time results
============================================
the confining stresss is  99961.44724326974
the deviatoric stress is  47379.85390898195
the volluntric strain is  0.038975068542271285

============================================
the confining stresss is  100010.61311042638
the deviatoric stress is  63980.74230693959
the volluntric strain is  0.034846007220645514
Shearing Finished!

============================================
the confining stresss is  100072.1646219381
the deviatoric stress is  48442.01921925928
the volluntric strain is  0.03260621667082442
  


! one thread the result is determinstic 
the confining stresss is  100021.99761514361
the deviatoric stress is  50314.69045611935
the volluntric strain is  0.03210116763725285
Shearing Finished!

============================================
the confining stresss is  100021.99761514361
the deviatoric stress is  50314.69045611935
the volluntric strain is  0.03210116763725285
Shearing Finished!

============================================
the confining stresss is  100080.66058013009
the deviatoric stress is  42349.78239322628
the volluntric strain is  0.029710046967979187


*loose sand 
============================================
the confining stresss is  100073.75779617828
the deviatoric stress is  47471.22225309565
the volluntric strain is  -0.030638498925959795
Shearing Finished!

the confining stresss is  99997.51300867967
the deviatoric stress is  43514.813248704886
the volluntric strain is  -0.026463079829254194
Shearing Finished!

the confining stresss is  100086.29308415836
the deviatoric stress is  49590.75807399175
the volluntric strain is  -0.03155698835490556
Shearing Finished!


def definePlot():
        plot.plots = {
                ' i ': ('unbalForce', 'a_strain'),
                'a_strain': ('volumtric_strain' ),
                # energy plot
                'a_strain ': ('d_stress', 'c_stress', None, 'q', 'p'),
                'a_strain  ': ('coordNum', 'fabricT'),
        }
"""