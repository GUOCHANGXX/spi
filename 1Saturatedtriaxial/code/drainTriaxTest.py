"""
# 
#  @author CHEN Qing
# 
#  Compilation:  ---
#  Execution:    
#       in the HPC: singularity run <image> myyade -j12 -n drainTriaxTest.py
#  Dependencies: myyade ( self compilation version and the alias is myyade )
#
#  drained triaxial shearing test (for pratice purpose)
#
"""
  
from yade import pack,  plot, utils, os# qt,


"""
Control the simulation
"""
## for readable
kPa = 1e3
MPa = 1e6

## save file 
#saveFolder = '/test'
#try:
#    os.mkdir(saveFolder)
#except OSError as error:
#    print(error) 
##os.mkdir(saveFolder)
#currPath = os.getcwd()
#savePath = currPath + saveFolder

#/**************************************************************************
""" define material , a.k.a., contact model   """   
#**************************************************************************/
sDensity = 2650
hDensity  = 900
youngModule = 3e9
poissonRatio = 0.2
packFricAngle = 5       # degree 5 or smaller for generate sample and 30 or higher for loose sample
realFricAngle = 30  

parMat = O.materials.append(CohFrictMat(
        density = sDensity,
        young=youngModule,
        poisson=poissonRatio,
        frictionAngle=radians(packFricAngle),
        isCohesive=False,
        normalCohesion = 6 * MPa,
        shearCohesion = 6 * MPa,# test this value tomorrow
        momentRotationLaw=False,
        alphaKr = 0.5,
        alphaKtw = 0.5,
        etaRoll = 0.5,
        etaTwist = 0.5,
        label="parMat")  )
hydMat = O.materials.append(CohFrictMat(
        density = hDensity,
        young=youngModule*0.1,
        poisson=poissonRatio,
        frictionAngle=radians(packFricAngle),
        isCohesive=False,
        normalCohesion = 6 * MPa,
        shearCohesion = 6 * MPa,# test this value tomorrow
        momentRotationLaw=False,
        alphaKr = 0.5,
        alphaKtw = 0.5,
        etaRoll = 0.5,
        etaTwist = 0.5,
        label="hydMat")  )
#// may be try to use FrictMatCDM or some bonded model

#/**************************************************************************
"""      define boundary condition        """  
#**************************************************************************/
## control triaxial process
sigmaIso = -1 * MPa
threhold_confi = 0.90
compressRate = 1
shearingRate = 1e-1
maxUbalForce = 1.0e-3
rStolerance = 1.0e-3
# relative tolerance between target  and actual stress

target_astrain = -0.50
saveFileNum = 100
saveStrainInterval = (-target_astrain)/saveFileNum 


#/**************************************************************************
"""      define geometry of the sample         """
#**************************************************************************/
## control box size 
height = 3.5
width  = 3.5
length = 3.5
## control pack void ratio / porosity
v_ratio = 0.68
n = v_ratio/(1+v_ratio)
n = 0.43

Sh = 0.01 * 00
psHyd = 0.04
hydMode = "poreFill"

## control particle size distribution
R_factor = 1.00
pS = [	0.100 * R_factor ,
        0.110 * R_factor ,
        0.120 * R_factor ,
        0.130 * R_factor ,
        0.140 * R_factor ,
        0.150 * R_factor ,
        0.170 * R_factor ,
        0.200 * R_factor ,
        0.230 * R_factor ,
        0.240 * R_factor ,
        0.250 * R_factor
]

pC = [	0.0,
	0.015625,
	0.046875,
	0.09375,
	0.1875,
	0.3125,
	0.484375,
	0.671875,
	0.84375,
	0.9375,
	1.000
]

   


""" 
control sample setup
"""

## generate particles and pack 
O.periodic = True
sp = pack.SpherePack()
minCon,maxCon=Vector3(0,0,0),Vector3(length,width,height)
snum=sp.makeCloud(minCon,maxCon,psdSizes=pS,psdCumm=pC,porosity=n,periodic=True,seed=3407)
#sp.toSimulation()
#O.bodies.append([sphere(center,rad,material='parMat') for center,rad in sp])
sp.toSimulation(material='parMat',color=(0,1,0))
#O.step()
#print(len(O.bodies))
#print(len(O.interactions))
#for i in O.interactions:
#        print(i.id1)



""" 
control boundary condition 
"""


## define triaxial controller
denseAssembly = PeriTriaxController(
                label='triax',
                # specify target values and whether they are strains or stresses
                # x,y,z is 3 binary bit, and 1 for stress 0 for strain 
                # so, all stress is 111 = 2**2 + 2**1 +2**0 = 4 + 2 + 1
                goal=(sigmaIso*threhold_confi, sigmaIso*threhold_confi, sigmaIso*threhold_confi),
                stressMask=7,
                # type of servo-control
                dynCell=True,
                maxStrainRate=(10*compressRate, 10*compressRate, 10*compressRate),
                # wait until the unbalanced force goes below this value
                maxUnbalanced=10*maxUbalForce,
                relStressTol=10*rStolerance,
                # call this function when goal is reached and the packing is stable
                doneHook='compression()'
        )
 

## define engines
O.engines = [
        ForceResetter(),
        InsertionSortCollider([Bo1_Sphere_Aabb(aabbEnlargeFactor=1.2)]),
        #InsertionSortCollider([Bo1_Sphere_Aabb()]),
        InteractionLoop(
            [Ig2_Sphere_Sphere_ScGeom6D(interactionDetectionFactor=1.2)], 
            #[Ig2_Sphere_Sphere_ScGeom6D()], 
            [Ip2_CohFrictMat_CohFrictMat_CohFrictPhys(setCohesionNow=True)], 
            [Law2_ScGeom6D_CohFrictPhys_CohesionMoment(
                always_use_moment_law=True,
		        useIncrementalForm=True)] 
        ),
        GlobalStiffnessTimeStepper(timeStepUpdateInterval=100,timestepSafetyCoefficient=0.8),
        denseAssembly,
        NewtonIntegrator(damping=.2),
        #PyRunner(command='history()', iterPeriod=5000),
]
#O.dt = .5 * PWaveTimeStep()
O.step()
## enable energy tracking in the code
O.trackEnergy = True

 

########################################################################################################
'''below this line is some functions that would call by the simulation'''
########################################################################################################
# there is a bug , that the vol should not be a constant, write a getVol() to get it 
def getVol():
        samVol = height * width * length
        return samVol

def getID(psh,hydMode):
        idFile = hydMode+str(int(100*getSh(psh)))+ "_" + str(int(100*getSh(psh) % 1)) +'.xml.bz2'    
        return idFile      


def checkParticle():
        print("============================================")
        #if(snum == len(O.bodies)):
        print(len(O.bodies),"particles have been generate.")
checkParticle()

def checkInteraction():
        print("============================================")
        realIntCount = 0
        activeCount = 0
        for i in O.interactions:
                if (i.isReal):
                        realIntCount += 1
                if (i.isActive):
                        activeCount += 1
                tmpIntr1 = i.id1
                tmpIntr2 = i.id2
                #print(i.phys.normalAdhesion, "is some contact's normal cohesion")
        print(realIntCount, "real interaction have been generate")
        print(activeCount, "active interaction have been generate")
        #return realIntCount

def getSh(pshyd):
        """ get and return current Hydrate Saturation """
        check_radius = pshyd/2
        vol = getVol()   
        pVol = 0.0 # all particle volume
        sVol = 0.0        # soil  volume
        hVol = 0.0     # hydrate  volume
        for b in O.bodies:
                pVol = pVol + 4/3*pi*b.shape.radius**3
                if ( b.shape.radius == check_radius):
                        hVol = hVol + 4/3*pi*b.shape.radius**3
                else:
                        sVol = sVol + 4/3*pi*b.shape.radius**3
        Sh = hVol / ( porosity() * vol ) #hVol / ( vol - pVol )
        #Sh = hVol / ( vol - sVol )
        #print("The saturation of hydrate is ", hVol/(vol-sVol) )
        return Sh



def ShParticleGenerator(sh=0.2,psh = 0.04, hyMor = "poreFill"):
        """ 
        generate a cetain mount of hydrate to with specify size and saturation in pore fill or cement morphology 

        input: 
                sh = 0.2 (default), saturation of hydrate
                psh = 0.04 (default), particle size of hydrate
                hyMor = "poreFill" , or "cement"
        """
        print("============== target Sh is ", sh, "==================")
        print("============== current Sh is ", getSh(psh), "==================")
        print("============== specify Sh size is ", psh, "==================")
        print("============== hydrate distrubution way is ", hyMor, "==================")
        # search whethe exit pack file
        packfile = 'Pack' + getID(psHyd,hydMode) 
        #import os.path 
        #if ( os.path.isfile(packfile) ):
        #        print("============== pack file exite, load, and check ==================")
        #        O.load(packfile)
        #else:
        print("============== pack file need to generate , now generate ==================")
        if ( hyMor == "poreFill" ) : 
                genHydra_poreFill(sh,psh)
        elif ( hyMor == "cement" ):
                genHydra_cemented(sh,psh)
        else : 
                print("Error! Not support such hydrate morphology.")
        O.save(packfile)
        print("! pack file saved==================")
        # save generate work
        # may should implement check whether existed pack file
        #packfile = 'Pack' + getID(psHyd,hydMode)            
        
 
def genHydra_poreFill(sh,psh):
        
        porVol = porosity() * (height*width*length)
        hydrateVol = sh * (porVol)
        import random
        random.random()
        r = psh / 2.0
        while( getSh(psh) < sh ):
                x = width * random.random()
                y = length * random.random()
                z = height * random.random()
                p = Vector3(x,y,z)
                conCheck = False
                for b in O.bodies:
                        dist = (p - b.state.pos).norm()
                        if (dist < r + b.shape.radius):
                                conCheck = True
                if (not conCheck):#generated
                        s = sphere(p,r,material="hydMat",color=(0,0,1))
                        O.bodies.append(s) 
        print("Current saturation of hydrate is ", getSh(psh) )
"""
def genHydra_poreFill(sh,psh):
        helper method to generate hydrate particle
        print(" pore fill function is runnig")
        porVol = porosity() * (height*width*length)
        hydrateVol = sh * (porVol)
        import random
        random.random()
        r = psh / 2.0
        while( getSh() < sh ):
                for i in O.interactions:
                        print(i.id1)
                        b1 = O.bodies[i.id1]
                        b2 = O.bodies[i.id2]
                        predHybol=pack.inHyperboloid(centerBottom=b1.state.pos,centerTop=b2.state.pos,radius=min(b1.shape.radius,b2.shape.radius),skirt=(min(b1.shape.radius,b2.shape.radius)/2.0))
                        predPar1 = pack.inSphere(center = b1.state.pos, radius = b1.shape.radius)
                        predPar2 = pack.inSphere(center = b2.state.pos, radius = b2.shape.radius)
                        pred = pack.PredicateDifference(predHybol,predPar1)
                        pred = pack.PredicateDifference(pred,predPar1)
                        spheres=pack.randomDensePack(pred,spheresInCell=2000,radius=r)
                        O.bodies.append(spheres)
        print("Current saturation of hydrate is ", getSh() )
"""
#generateHydrate( saturation=0.40,size=)


def compression():
        print("============================================")
        checkInteraction()
        
        print('initial assembly have generated')
        print('the confining stresss is ', ( triax.stress[0]+triax.stress[1]+triax.stress[2])/3 )
        print('the friction of assembly is ', (O.materials['parMat'].frictionAngle))
        O.materials['parMat'].frictionAngle=radians(realFricAngle)# it work, no need to specify the particle material
        print('the friction of assembly is changed to ', (O.materials['parMat'].frictionAngle))

        triax.goal=(sigmaIso, sigmaIso, sigmaIso)
        triax.stressMask=7
        triax.dynCell=True
        triax.maxStrainRate=(compressRate, compressRate, compressRate)
        # wait until the unbalanced force goes below this value
        triax.maxUnbalanced=maxUbalForce
        triax.relStressTol=rStolerance
        # call this function when goal is reached and the packing is stable
        triax.doneHook='shear()'

def genHydrate():
        print('initial assembly have consolidated.Now generate hydrate particle:')
        ShParticleGenerator(sh=Sh,psh = psHyd, hyMor = hydMode)
        #O.pause()
        #triax.doneHook='shear()'
        #O.pause()

def shear():
        print("============================================")
        print('the confining stresss is ', ( triax.stress[0]+triax.stress[1]+triax.stress[2])/3 )
        print('the friction of assembly is ', (O.materials['parMat'].frictionAngle))
        genHydrate()
        print('shearing begin:')
        #O.materials['parMat'].alphaKr=2.0
        #O.materials['parMat'].alphaKtw=2.0
        #O.materials['parMat'].etaRoll=0.3
        #O.materials['parMat'].etaTwist=0.3
        #O.materials['parMat'].normalCohesion = 1e20
        #O.materials['parMat'].shearCohesion = 1e20
        #O.materials['parMat'].momentRotationLaw=True
        
        # trsf: Current transformation matrix of the cell
        O.cell.trsf = Matrix3.Identity
        # change control type: keep constant confinement in x,y, target axial strain compression in z
        initAstrainFactor = 1e-5
        triax.goal = (sigmaIso, sigmaIso, initAstrainFactor*target_astrain)
        #triax.goal = (sigmaIso, sigmaIso, target_astrain)
        triax.stressMask = 3
        triax.maxStrainRate = (shearingRate, shearingRate, 0.1*shearingRate)
        triax.doneHook = 'trueShear()'
        triax.maxUnbalanced = maxUbalForce
        triax.relStressTol = rStolerance
 

def trueShear():
        print('Ture shearing begin, initial state all check and save:')
        triax.goal = (sigmaIso, sigmaIso, target_astrain)
        history()
        saveData()
        O.engines += [PyRunner(command='history()',iterPeriod=1000)]
        O.engines += [PyRunner(command='saveData()',iterPeriod=1000)]
        triax.doneHook='triaxFinished()'


def pFromStressTensor():
        p = 0
        Tstress = utils.normalShearStressTensors()[0]+utils.normalShearStressTensors()[1]
        p = (Tstress[0][0]+Tstress[1][1])/2.0
        return p

def qFromStressTensor():
        q = 0
        Tstress = utils.normalShearStressTensors()[0]+utils.normalShearStressTensors()[1]
        q = Tstress[2][2] - (Tstress[0][0]+Tstress[1][1])/2.0
        return q

def history():
	plot.addData(
		e11=triax.strain[0], 
		e22=triax.strain[1], 
		e33=triax.strain[2],
		s11=triax.stress[0],
		s22=triax.stress[1],
		s33=triax.stress[2],
		i=O.iter,
		a_strain = -triax.strain[2],
		c_stress = -(triax.stress[0]+triax.stress[1])/2,
		d_stress = -(triax.stress[2] - ((triax.stress[0]+triax.stress[1])/2.0) ),
		volumtric_strain = ( triax.strain[2]+triax.strain[1]+triax.strain[0] ) ,
                unbalForce = triax.currUnbalanced,
                poro = utils.porosity(),
                coordNum = utils.avgNumInteractions(),
                stressTn = utils.normalShearStressTensors()[0],
                stressTs = utils.normalShearStressTensors()[1],
                fabricT = utils.fabricTensor()[0],
                p = pFromStressTensor(),
                q = qFromStressTensor()
                )

## define what to plot
def definePlot():
        plot.plots = {
                ' i ': ('unbalForce', 'a_strain'),
                'a_strain': ('volumtric_strain' ),
                # energy plot
                'a_strain ': ('d_stress', 'c_stress', None, 'q', 'p'),
                'a_strain  ': ('d_stress', 'c_stress'),
        }

## save the plot data
def savePlot():
    #savefile = 'dataFile.txt.tar.gz'
    savefile = 'data' + getID(psHyd,hydMode)
    plot.saveDataTxt(savefile,vars=(
        'a_strain','volumtric_strain','c_stress','d_stress','poro','unbalForce','coordNum'
    ))

savingNum = 0
savingStrain = 0.0
####################################################
def saveData():
        global savingNum
        global savingStrain
        # while the axial increase saveStrainInterval%, then save 
        if ( triax.strain[2] < savingStrain ):
            #savePlot()
            #O.save(savePath + '/shear'+str(savingNum)+'.xml.bz2')
            #O.save('shear'+str(savingNum)+'.xml.bz2')
            shearfile = 'shear'+str(savingNum) + getID(psHyd,hydMode)
            O.save(shearfile)
            savePlot()
            print('point at strain',str(savingStrain),'saved.')
            savingNum += 1
            savingStrain = -saveStrainInterval * savingNum
            
        
#saveData()

def triaxFinished():
        print("============================================")
        print('the confining stresss is ', (-(triax.stress[0]+triax.stress[1])/2) )
        print('the deviatoric stress is ', -(triax.stress[2] - ((triax.stress[0]+triax.stress[1])/2.0) ) )
        print('the volluntric strain is ', ( triax.strain[2]+triax.strain[1]+triax.strain[0] ) )
        print('Shearing Finished!')
        # save the plot
        savePlot()
        # show the plot
        definePlot()
        plot.plot(subPlots=False)
        O.pause()
        O.save('periodiTriaixal.yade.bz2')

#O.run()
O.run(wait=True)
 
