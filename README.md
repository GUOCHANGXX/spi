# Topic

### Unsaturated-Soil Pipe Interaction Modelling by DEM

# Target

#### To simulate soil-pipe interaction in axial and lateral direction under unsaturated conditions

##### The first step is to study the axial resistence.

# Plan

#### 1. To re-achieve [Meidani, 2017](Reference/Meidani, Meguid and Chouinard-2017-Evaluation.pdf)

#### 2. Achieve strain analysis in triaxial modelling

#### 3. To verify model by direct shear and interface direct shear tests 

#### 4. to calculate pipe's load, stress distribution and add overlaying pressure by a new boundary method, try a particle creating method

#### 5. To verify the literature unsaturated sand and also existing triaxial testing results

------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------


### Below is left as example for editing .md file until I'm familiar.
 Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.polyu.edu.hk/21040974r/spi_dem.git
git branch -M main
git push -uf origin main
```
