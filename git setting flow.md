# How to set gitlab?

```
g@guo:~/.ssh$ ssh-keygen -t ed25519 -C "chang.guo@connect.polyu.hk"
Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/g/.ssh/id_ed25519):  
```

########
Enter button
########
```
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/g/.ssh/id_ed25519
Your public key has been saved in /home/g/.ssh/id_ed25519.pub
The key fingerprint is:
SHA256:eZj+ejTwICbrse6i0ioIJjJ5BJFOgjnAMFGaXW871E8 chang.guo@connect.polyu.hk
The key's randomart image is:
+--[ED25519 256]--+
|@*. .            |
|BB . . .         |
|*o.   + . E      |
| .. .oo.o*       |
| o   +o.S+o      |
|*.. o  o .+      |
|*+ . o  .. .     |
|+ o o    ..      |
|=o +o   .o.      |
+----[SHA256]-----+
```

```
g@guo:~/.ssh$ gedit id_ed25519.pub 
```

########
Copy to giblab
########
```
g@guo:~/.ssh$ ssh -T git@gitlab.com
Warning: Permanently added 'gitlab.com,172.65.251.78' (ECDSA) to the list of known hosts.
Welcome to GitLab, @PolyU_C.GUO!
```

https://stackoverflow.com/questions/47860772/gitlab-remote-http-basic-access-denied-and-fatal-authentication

glpat-V5wD_1ABvRye6hsnA6Yt


```
g@guo:~/myYade$ git clone https://GC:glpat-V5wD_1ABvRye6hsnA6Yt@gitlab.polyu.edu.hk/C.GUO_PolyU/spi_dem.git
正克隆到 'spi_dem'...
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
展开对象中: 100% (3/3), 2.82 KiB | 2.82 MiB/s, 完成.
```



g@guo:~/myYade/spi_dem$ git config --global credential.helper store


