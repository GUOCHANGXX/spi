import qt, plot
O.periodic = True

OutDiameter=0.1143
PipeRoughR=0.01
SectionNum=20
LongNum=15
WidthX=0.6
HeightZ=3
HeightZ0=0.3
thickness=0.01
gravity=(0,0,-10)
densityPipe=7850

SphereMID=O.materials.append(FrictMat(young=150e6,poisson=0.25,frictionAngle=radians(34),density=2700,label='spheres'))
SideMID=O.materials.append(FrictMat(young=1.9e11,poisson=0.3,frictionAngle=0,density=7860,label='side'))
BottomMID=O.materials.append(FrictMat(young=1.9e11,poisson=0.3,frictionAngle=0,density=7860,label='Bottom'))
PipeMID=O.materials.append(FrictMat(young=2.09e11,poisson=0.3,frictionAngle=0,density=densityPipe,label='Pipe'))

InputR=OutDiameter/2-PipeRoughR
Gap=sin(pi/SectionNum)*2*InputR
LengthY=Gap*(LongNum-1)+PipeRoughR

PipeParticleNum=0
PP=[]
for j in range(LongNum):
    for i in range(SectionNum):
        PipeParticleNum += 1
        PP.append(sphere([InputR*sin(i*2*pi/SectionNum)+0.3,j*Gap,InputR*cos(i*2*pi/SectionNum)+0.3],radius=PipeRoughR,material=O.materials[PipeMID],color=(1,1,0)))

PPID=O.bodies.appendClumped(PP)
PPClump=O.bodies[PPID[0]]
# PPClump.fixed=1
PPClump.dynamic=1
PPClump.state.blockedDOFs='xyXYZ'
PPClump.state.vel = (0, 1,0)
PPClump.state.angVel = (0,0,0)
for b in O.bodies:
	if b.isClumpMember:
		b.state.blockDOFs='xyXYZ'

O.cell.hSize=Matrix3(WidthX,0,0,0,LengthY,0,0,0,HeightZ)
h=0.4
sp=pack.SpherePack()
sp.makeCloud((thickness*5,0.001,h),(WidthX-thickness*5,LengthY-0.001,h+.2),rMean=0.02)
#
sp.toSimulation(material=O.materials[SphereMID])

newton=NewtonIntegrator(damping=0.4,gravity=gravity,dampGravity=0)
O.engines=[
	ForceResetter(),
	#(1) This is where we allow big bodies, else it would crash due to the very large bottom box:
	InsertionSortCollider([Bo1_Box_Aabb(),Bo1_Sphere_Aabb()],allowBiggerThanPeriod=True),
	InteractionLoop(
		[Ig2_Sphere_Sphere_ScGeom(),Ig2_Box_Sphere_ScGeom()],
		[Ip2_FrictMat_FrictMat_FrictPhys()],
		[Law2_ScGeom_FrictPhys_CundallStrack()]
	),
	GlobalStiffnessTimeStepper(),
    newton,
    PyRunner(command='PT()',iterPeriod=100,label='checker'),


]


def PT():
    Fsum=Vector3()
    for b in O.bodies:
        if b.isClumpMember:
            Fsum+=O.forces.f(b.id)
    yy=PPClump.state.pos
    # print(Fsum)
    plot.addData(t=O.time,Fsum0=Fsum[0],Fsum1=Fsum[1],Fsum2=Fsum[2],xx=yy[0],yy=yy[1],zz=yy[2])

def Zfreer():
    Fsum = Vector3()
    for b in O.bodies:
        if b.isClumpMember:
            Fsum+=O.forces.f(b.id)
    dv = Vector3(0,0,(Fsum[2]/(densityPipe*LengthY*pi*(OutDiameter**2/4-(OutDiameter-PipeRoughR)**2/4))+gravity[2])*O.dt)
    PPClump.state.vel+=dv
    # print(Fsum)

for b in O.bodies:
    if isinstance(b.shape,Sphere):
        b.state.vel[2]=-1

plot.plots = {'t':('Fsum0','Fsum1','Fsum2',),'t ':('xx',),'t  ':('yy',),' t':('zz',)}

plot.plot()

qt.View()

O.run()
