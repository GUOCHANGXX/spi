import qt, plot
O.periodic = True

OutDiameter=0.1143
PipeRoughR=0.004
SectionNum=50
LongNum=75
WidthX=0.6
HeightZ=2.2
HeightZ0=0.3
thickness=0.01
gravity=(0,0,-9.8)
densityPipe=7850
Stable=0
stabilityThreshold=0.035

SphereMID=O.materials.append(FrictMat(young=150e6,poisson=0.25,frictionAngle=radians(34),density=2700,label='spheres'))
SideMID=O.materials.append(FrictMat(young=1.9e20,poisson=0.3,frictionAngle=0,density=7860,label='side'))
BottomMID=O.materials.append(FrictMat(young=1.9e20,poisson=0.3,frictionAngle=0,density=7860,label='Bottom'))
PipeMID=O.materials.append(FrictMat(young=2.09e11,poisson=0.3,frictionAngle=radians(34*0.8),density=densityPipe,label='Pipe'))

InputR=OutDiameter/2-PipeRoughR
Gap=sin(pi/SectionNum)*2*InputR
LengthY=Gap*(LongNum-1)+PipeRoughR

# filename='particle.txt'
# with open (filename) as file_object:
#     for line in file_object:
#         data=line.split()
#         center=Vector3(float(data[0]),float(data[1]),float(data[2]))
#         radius=float(data[3])
#         if (((float(data[0])-WidthX/2)**2+(float(data[2])-HeightZ0)**2)**(1/2)-OutDiameter/2-radius+0.5*radius)>0:
#             O.bodies.append(sphere(center,radius,material=O.materials[SphereMID]))

h=0.06
sp=pack.SpherePack()
sp.makeCloud((thickness*5,0.001,h),(WidthX-thickness*5,LengthY-0.001,h+2.04),rMean=0.02)
sp.toSimulation()
Stage=0 # 0 press 1 unloading 2 erase particle and install pipe 3 stablizing 4loading

ColorBox=(255,218,185)
NorCB=(ColorBox[0]/sum(ColorBox),ColorBox[1]/sum(ColorBox),ColorBox[2]/sum(ColorBox),)

O.cell.hSize=Matrix3(WidthX,0,0,0,LengthY,0,0,0,HeightZ)

sumV=0
for b in O.bodies:
    if isinstance(b.shape,Sphere):
        sumV+=4/3*pi*(b.shape.radius)**3
print(sumV)

lowBox = box(center=(WidthX/2.0,LengthY/2.0,thickness),extents=(WidthX,LengthY,thickness/2),fixed=True,dynamic=0,color=(1,0,0),material=O.materials[BottomMID])
sideBox1 = box(center=(thickness,LengthY/2,HeightZ/2),extents=(thickness/2,LengthY,HeightZ),fixed=True,dynamic=0,color=NorCB,material=O.materials[SideMID])
sideBox2 = box(center=(WidthX-thickness,LengthY/2,HeightZ/2),extents=(thickness/2,LengthY,HeightZ),fixed=True,dynamic=0,color=NorCB,material=O.materials[SideMID])
Pressor = box(center=(WidthX/2.0,LengthY/2.0,
                      HeightZ-thickness/2),extents=(WidthX,LengthY,thickness/2),fixed=True,dynamic=0,color=(1,0,0),material=O.materials[BottomMID])
lowid=O.bodies.append([lowBox])
pressorid=O.bodies.append(Pressor)
PR=O.bodies[pressorid]
h=1
PR.state.vel[2]=-2
newton=NewtonIntegrator(damping=0.4,gravity=(0,0,-9.8),dampGravity=0)
O.engines=[
	ForceResetter(),
	#(1) This is where we allow big bodies, else it would crash due to the very large bottom box:
	InsertionSortCollider([Bo1_Box_Aabb(),Bo1_Sphere_Aabb()],allowBiggerThanPeriod=True),
	InteractionLoop(
		[Ig2_Sphere_Sphere_ScGeom(),Ig2_Box_Sphere_ScGeom()],
		[Ip2_FrictMat_FrictMat_FrictPhys()],
		[Law2_ScGeom_FrictPhys_CundallStrack()]
	),
	GlobalStiffnessTimeStepper(timestepSafetyCoefficient=0.5),
	newton,
    PyRunner(command='PT()', iterPeriod=200, label='PT',dead=1),
    PyRunner(command='check()', iterPeriod=2000, label='checker')
]

def Stabler():
    O.engines[-2].dead = 0
    PPClump.state.vel = (0, 0.01, 0)
    PPClump.state.angVel = (0, 0, 0)
    global Stable
    Stable=1

def check():
    global Stage
    if Stage==0:
        if PR.state.pos[2]<0.3:
            Stage = 1
            PR.state.vel[2]=.2
            print('stage0 end, Pressor pos and speed are '+str(PR.state.pos[2])+' and '+str(PR.state.vel[2]))
    elif Stage == 1:
        if PR.state.pos[2]>1.2:
            Stage =2
            PR.state.vel[2] = 0
            print('stage1 end, Pressor pos and speed are ' + str(PR.state.pos[2]) +' and '+ str(PR.state.vel[2]))
    elif Stage == 2:
        eraseparticles()
        addPipe()
        Stage =3
        nn=0
        for b in O.bodies:
            if isinstance(b.shape,Sphere):
                nn+=1
        print('stage2 end, pipe particles number is ' + str(nn))
    elif Stage ==3:
        if (unbalancedForce() < stabilityThreshold) and (abs(PipeForce()[0])<20):
            Stage = 4
            Stabler()
            print('stage3 end,pipe pos and speed are ' + str(PPClump.state.pos[1])+' and '+str(PPClump.state.vel[1]))
    elif Stage ==4:
        if PPClump.state.pos[1]>(LengthY/2+0.05):
            O.pause()
            print('end')

def PT():
    Fsum=PipeForce()
    if Stable == 1:
        plot.addData(y=PPClump.state.pos[1]-LengthY/2,Horizontal=Fsum[0],Axial=Fsum[1],Vertical=Fsum[2],zz=PPClump.state.pos[2])

def PipeForce():
    Fsum = Vector3()
    for b in O.bodies:
        if b.isClumpMember:
            Fsum += O.forces.f(b.id)
    return Fsum

def addPipe():
    global PPID,PPClump
    PipeParticleNum = 0
    PP = []
    for j in range(LongNum):
        for i in range(SectionNum):
            PipeParticleNum += 1
            PP.append(sphere(
                [InputR * sin(i * 2 * pi / SectionNum) + 0.3, j * Gap, InputR * cos(i * 2 * pi / SectionNum) + 0.3],
                radius=PipeRoughR, material=O.materials[PipeMID], color=(1, 1, 0)))

    PPID = O.bodies.appendClumped(PP)
    PPClump = O.bodies[PPID[0]]
    PPClump.dynamic = 0
    PPClump.state.blockedDOFs = 'yXYZ'

def eraseparticles():
    print('len(O.bodies)' + str(len(O.bodies)))
    nnn = 0
    for b in O.bodies:
        if isinstance(b.shape, Sphere):
            if (((b.state.pos[0] - WidthX / 2) ** 2 + (b.state.pos[2] - HeightZ0) ** 2) ** (
                    1 / 2) - OutDiameter / 2 - 0.5 * b.shape.radius) < 0:
                nnn += 1
                O.bodies.erase(b.id)
    print('erose number = '+str(nnn))
    print('len(O.bodies) = ' + str(len(O.bodies)))



plot.plots = {'y':('Horizontal'),' y':('Axial'),'y ':('Vertical',),'y  ':('zz',)}

plot.plot()

qt.View()

O.run()
