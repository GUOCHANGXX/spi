import qt, plot
O.periodic = True

OutDiameter=0.1143
PipeRoughR=0.01
SectionNum=20
LongNum=15
WidthX=0.6
HeightZ=1
HeightZ0=0.3
thickness=0.01

SphereMID=O.materials.append(FrictMat(young=150e6,poisson=0.25,frictionAngle=radians(34),density=2700,label='spheres'))
SideMID=O.materials.append(FrictMat(young=1.9e11,poisson=0.3,frictionAngle=0,density=7860,label='side'))
BottomMID=O.materials.append(FrictMat(young=1.9e11,poisson=0.3,frictionAngle=0,density=7860,label='Bottom'))
PipeMID=O.materials.append(FrictMat(young=2.09e11,poisson=0.3,frictionAngle=0,density=7850,label='Pipe'))

InputR=OutDiameter/2-PipeRoughR
Gap=sin(pi/SectionNum)*2*InputR
LengthY=Gap*(LongNum-1)+PipeRoughR

filename='particle.txt'
with open (filename) as file_object:
    for line in file_object:
        data=line.split()
        center=Vector3(float(data[0]),float(data[1]),float(data[2]))
        radius=float(data[3])
        if (((float(data[0])-WidthX/2)**2+(float(data[2])-HeightZ0)**2)**(1/2)-OutDiameter/2-radius+0.5*radius)>0:
            O.bodies.append(sphere(center,radius,material=O.materials[SphereMID]))

PipeParticleNum=0
PP=[]
for j in range(LongNum):
    for i in range(SectionNum):
        PipeParticleNum += 1
        PP.append(sphere([InputR*sin(i*2*pi/SectionNum)+0.3,j*Gap,InputR*cos(i*2*pi/SectionNum)+0.3],radius=PipeRoughR,material=O.materials[PipeMID],color=(1,1,0)))

PPID=O.bodies.appendClumped(PP)
PPClump=O.bodies[PPID[0]]
# PPClump.fixed=1
PPClump.dynamic=0
PPClump.state.vel[1]=0.1
# PPClump.state.blockDOFs='xyXYZ'

ColorBox=(255,218,185)
NorCB=(ColorBox[0]/sum(ColorBox),ColorBox[1]/sum(ColorBox),ColorBox[2]/sum(ColorBox),)

O.cell.hSize=Matrix3(WidthX,0,0,0,LengthY,0,0,0,HeightZ)

sumV=0
for b in O.bodies:
    if isinstance(b.shape,Sphere):
        sumV+=4/3*pi*(b.shape.radius)**3
print(sumV)

lowBox = box(center=(WidthX/2.0,LengthY/2.0,thickness),extents=(WidthX,LengthY,thickness/2),fixed=True,dynamic=0,color=(1,0,0),material=O.materials[BottomMID])
sideBox1 = box(center=(thickness,LengthY/2,HeightZ/2),extents=(thickness/2,LengthY,HeightZ),fixed=True,dynamic=0,color=NorCB,material=O.materials[SideMID])
sideBox2 = box(center=(WidthX-thickness,LengthY/2,HeightZ/2),extents=(thickness/2,LengthY,HeightZ),fixed=True,dynamic=0,color=NorCB,material=O.materials[SideMID])
Pressor = box(center=(WidthX/2.0,LengthY/2.0,.95),extents=(WidthX,LengthY,thickness/2),fixed=True,dynamic=0,color=(1,0,0),material=O.materials[BottomMID])
lowid,b1id,b2id=O.bodies.append([lowBox,sideBox1,sideBox2])
# pressorid=O.bodies.append(Pressor)
# PR=O.bodies[pressorid]
# PR.state.vel[2]=-0.1
newton=NewtonIntegrator(damping=0.4,gravity=(0,0,-10),dampGravity=0)
O.engines=[
	ForceResetter(),
	#(1) This is where we allow big bodies, else it would crash due to the very large bottom box:
	InsertionSortCollider([Bo1_Box_Aabb(),Bo1_Sphere_Aabb()],allowBiggerThanPeriod=True),
	InteractionLoop(
		[Ig2_Sphere_Sphere_ScGeom(),Ig2_Box_Sphere_ScGeom()],
		[Ip2_FrictMat_FrictMat_FrictPhys()],
		[Law2_ScGeom_FrictPhys_CundallStrack()]
	),
	GlobalStiffnessTimeStepper(),
	newton,
    PyRunner(command='PT()', iterPeriod=500, label='checker')
]


def PT():
    Fsum=Vector3()
    for b in O.bodies:
        if b.isClumpMember:
            Fsum+=O.forces.f(b.id)
    # print(Fsum)
    plot.addData(t=O.time,Fsum0=Fsum[0],Fsum1=Fsum[1],Fsum2=Fsum[2])

plot.plots = {'t':('Fsum0','Fsum1','Fsum2',),}

plot.plot()

qt.View()

O.run()
