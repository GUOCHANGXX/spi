O.periodic = True

OutDiameter=0.1143
PipeRoughR=0.01
SectionNum=20
LongNum=15
WidthX=0.6
HeightZ=3
HeightZ0=0.3
thickness=0.01

SphereMID=O.materials.append(FrictMat(young=150e6,poisson=0.25,frictionAngle=radians(34),density=2700,label='spheres'))
SideMID=O.materials.append(FrictMat(young=1.9e11,poisson=0.3,frictionAngle=0,density=7860,label='side'))
BottomMID=O.materials.append(FrictMat(young=1.9e11,poisson=0.3,frictionAngle=0,density=7860,label='Bottom'))
PipeMID=O.materials.append(FrictMat(young=2.09e11,poisson=0.3,frictionAngle=0,density=7850,label='Pipe'))



InputR=OutDiameter/2-PipeRoughR
Gap=sin(pi/SectionNum)*2*InputR
LengthY=Gap*(LongNum-1)+PipeRoughR
print(LengthY)

# PipeParticleNum=0
# PP=[]
# for j in range(LongNum):
#     for i in range(SectionNum):
#         PipeParticleNum += 1
#         PP.append(sphere([InputR*sin(i*2*pi/SectionNum)+0.3,j*Gap,InputR*cos(i*2*pi/SectionNum)+0.3],radius=PipeRoughR,color=(1,1,0)))
#
# PPID=O.bodies.appendClumped(PP)
# PPClump=O.bodies[PPID[0]]
# PPClump.fixed=1
# PPClump.dynamic=0
# PPClump.state.blockDOFs='xyzXYZ'
#
# print(PipeParticleNum)


ColorBox=(255,218,185)
NorCB=(ColorBox[0]/sum(ColorBox),ColorBox[1]/sum(ColorBox),ColorBox[2]/sum(ColorBox),)

O.cell.hSize=Matrix3(WidthX,0,0,0,LengthY,0,0,0,HeightZ)
h=0.8
sp=pack.SpherePack()
sp.makeCloud((thickness*5,0.001,h),(WidthX-thickness*5,LengthY-0.001,h+2),rMean=0.02)
#
sp.toSimulation(material=O.materials[SphereMID])
sumV=0
for b in O.bodies:
    if isinstance(b.shape,Sphere):
        sumV+=4/3*pi*(b.shape.radius)**3
print(sumV)

lowBox = box(center=(WidthX/2.0,LengthY/2.0,thickness),extents=(WidthX,LengthY,thickness/2),fixed=True,dynamic=0,color=(1,0,0),material=O.materials[BottomMID])
sideBox1 = box(center=(thickness,LengthY/2,HeightZ/2),extents=(thickness/2,LengthY,HeightZ),fixed=True,dynamic=0,color=NorCB,material=O.materials[SideMID])
sideBox2 = box(center=(WidthX-thickness,LengthY/2,HeightZ/2),extents=(thickness/2,LengthY,HeightZ),fixed=True,dynamic=0,color=NorCB,material=O.materials[SideMID])
Pressor = box(center=(WidthX/2.0,LengthY/2.0,2.95),extents=(WidthX,LengthY,thickness/2),fixed=True,dynamic=0,color=(1,0,0),material=O.materials[BottomMID])
lowid,b1id,b2id,pressorid=O.bodies.append([lowBox,sideBox1,sideBox2,Pressor])
PR=O.bodies[pressorid]
PR.state.vel[2]=-0.1
newton=NewtonIntegrator(damping=0.4,gravity=(0,0,-10),dampGravity=0)
O.engines=[
	ForceResetter(),
	#(1) This is where we allow big bodies, else it would crash due to the very large bottom box:
	InsertionSortCollider([Bo1_Box_Aabb(),Bo1_Sphere_Aabb()],allowBiggerThanPeriod=True),
	InteractionLoop(
		[Ig2_Sphere_Sphere_ScGeom(),Ig2_Box_Sphere_ScGeom()],
		[Ip2_FrictMat_FrictMat_FrictPhys()],
		[Law2_ScGeom_FrictPhys_CundallStrack()]
	),
	GlobalStiffnessTimeStepper(),
	newton,
    PyRunner(command='PT()',iterPeriod=500,label='checker'),
]

def PT():
    maxz=0
    for b in O.bodies:
        if isinstance(b.shape,Sphere):
            if b.state.pos[2]+b.shape.radius > maxz:
                maxz=b.state.pos[2]+b.shape.radius
    print(1-sumV/(PR.state.pos[2]*WidthX*LengthY),1-sumV/(maxz*WidthX*LengthY),maxz)


for b in O.bodies:
    if isinstance(b.shape,Sphere):
        print(b.state.pos,b.shape.radius)

AAA1=[]
for b in O.bodies:
    if isinstance(b.shape,Sphere):
        AAA1.append([b.state.pos[0],b.state.pos[1],b.state.pos[2],b.shape.radius,-1])
AA=open(('particle.txt'),'w')
for i in range(len(AAA1)):
    AA.write(str(AAA1[i][0])+'\t'+str(AAA1[i][1])+'\t'+str(AAA1[i][2])+'\t'+str(AAA1[i][3])+'\t'+str(AAA1[i][4])+'\t'+'\n')
AA.close()

AAA2=[]

iii=0
jjj=0
for b in O.bodies:
    if isinstance(b.shape,Sphere) and b.isStandalone:
        iii+=1
        if (((b.state.pos[0]-WidthX/2)**2+(b.state.pos[2]-HeightZ0)**2)**(1/2)-OutDiameter/2-b.shape.radius)<0:
            jjj+=1
            O.bodies.erase(b.id)
print(iii,jjj)

